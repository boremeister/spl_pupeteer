const puppeteer = require("puppeteer");

// web elements
const url = "https://www.mojupravnik.si/";
const inputUsername = "#dnn_ctr477_ViewKiviRegistracija_WebLogin1_txtUsername";
const inputPassword = "#dnn_ctr477_ViewKiviRegistracija_WebLogin1_txtPassword";
const buttonLogin = "#dnn_ctr477_ViewKiviRegistracija_WebLogin1_btnLogin";
const buttonStanjeStevcev = "//a[@href='/1/Mojeenote/Oddajastanjastevcev.aspx']";
const inputColdWater = '#dnn_ctr502_ViewKiviObrazec_WebStanjaStevca1_WebVnosStanjeStevca1_txtStHV22';
const inputWarmWater = '#dnn_ctr502_ViewKiviObrazec_WebStanjaStevca1_WebVnosStanjeStevca1_txtTVStHV22';
const buttonPreview = "#dnn_ctr502_ViewKiviObrazec_WebStanjaStevca1_btnNext";

(async () => {

  // read script parameters
  const args = process.argv.slice(2)

  if (typeof args === 'undefined' || args.length < 4) {
    console.log("Missing parameters! Usage: node spl.js <username> <password> <coldWater> <warmWater>");
    return;
  }

  const username = args[0]  // username
  const password = args[1]  // password
  const coldWater = args[2] // cold water
  const warmWater = args[3] // warm water
  
  // launch browser
  const browser = await puppeteer.launch({
    headless: false, // launch browser in full browser
    args: [`--window-size=1366,768`],
    defaultViewport: null,
    //slowMo: 100 // slow down by 250ms
  });

  const page = await browser.newPage();
  await page.goto(url);

  // login
  await page.type(
    inputUsername,
    username
  ); // username
  await page.type(
    inputPassword,
    password
  ); // password

  // click login button
  await page.click(buttonLogin);

  // wait for link 'oddaj stanje števcev' and click it
  let stanjeStevcevButton = await page.waitForXPath(
    buttonStanjeStevcev,
    { visible: true }
  );

  // click "stanje števcev" button
  stanjeStevcevButton.click();

  // enter values
  await page.waitForSelector(
    inputColdWater,
    { visible: true }
  );

  await page.waitForSelector(
    inputWarmWater,
    { visible: true }
  );

  await page.type(inputColdWater, coldWater);
  // click other input so calculations can be done
  await page.click(inputWarmWater);
  await delay(1000);
  await page.type(inputWarmWater, warmWater);
  // click other input so calculations can be done
  await page.click(inputColdWater);
  await delay(1000);

  // click preview button
  await page.click(buttonPreview);

  // make snpashot
  await page.screenshot({path: 'snapshots/preview-' + getTodayDate() + '.png'});

})();

function delay(time) {
    return new Promise(function(resolve) { 
        setTimeout(resolve, time)
    });
 }

function getTodayDate(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    
    if(dd<10) {
        dd = '0'+dd
    } 
    
    if(mm<10) {
        mm = '0'+mm
    } 
    
    return yyyy + '-' + mm + '-' + dd;
}